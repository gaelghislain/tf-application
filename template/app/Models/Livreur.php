<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Livreur extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

     /*est cree par un utilisateur*/
	public function user()
	{
        return $this->belongsTo(User::class,'created_id');
    }

     public function livraisons()
    {
        return $this->hasMany(Liver_par::class);
    }

    public function users()
    {
        return $this->morphMany(User::class, 'usertable');
    }
}
