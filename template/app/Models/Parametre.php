<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Parametre extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
