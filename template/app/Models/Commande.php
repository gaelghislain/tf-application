<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Commande extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];
    //

     /*une commande appartient
    à un  ulisateur*/
    public function user(){

        return $this->belongsTo(User::class,'created_id');
      }

       /*une commande appartient
      à un  receptionnaire*/
      public function receptionnaire(){

        return $this->belongsTo(Receptionnaire::class,'receptionnaire_id');
      }

       /*une commande appartient
      à un  source*/
      public function source_cmd(){

        return $this->belongsTo(Source_Cmd::class,'source_id');
      }

      /*une commande appartient
      à un  mode*/
      public function mode_pai(){

        return $this->belongsTo(Mode_pai::class,'mode_id');
      }


       /*une commande appartient
      à un  type*/
      public function type_cmd(){

        return $this->belongsTo(Type_Cmd::class,'type_id');
      }

       /*une commande appartient
      à un  annomalie*/
      public function annomalie(){

        return $this->belongsTo(Anormalie::class,'id');
      }

      public  function client()
      {
          return $this->belongsTo(Client::class,'client_id');
      }

      public  function menu()
      {
          return $this->belongsTo(Menu::class,'menu_id');
      }

      public  function carnet_adresse()
      {
          return $this->belongsTo(Carnet_Adresse::class,'carnet_adresse_id');
      }


       public function livreur()
      {
          return $this->belongsTo(Livreur::class);
      }


       public function livraison()
      {
          return $this->belongsTo(Liver_par::class);
      }

      public function attribution(){

        return $this->belongsTo(Attibuer_Cmd::class,'commande_id');
      }
}
