<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Carnet_Adresse extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function client(){

      return $this->belongsTo(Client::class);
    }
}
