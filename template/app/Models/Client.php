<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Client extends Model
{
    use HasFactory, HasUuid;

    protected $casts = [
        'id' => 'string'
    ];

    protected $guarded = [];


    public function adresses()
    {
        return $this->hasMany(Carnet_Adresse::class, 'client_id');
    }
    public function commandes()
    {
        return $this->hasMany(Commande::class, 'client_id');
    }
    public function suggestions()
    {
        return $this->hasMany(Suggestion::class, 'client_id');
    }
    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'usertable_id');
    }
    /*est cree par un utilisateur*/
    public function coupons()
    {
        return $this->hasMany(Coupon::class);
    }
    public function users()
    {
        return $this->morphMany(User::class, 'usertable');
    }
    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }
    public function date_ventes()
    {
        return $this->hasMany(Date_vente::class);
    }
}
