<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Receptionnaire extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*un receptionnaire peut faire plusieur commande tandis qu'une commande appartient à un receptionnaire  alors la relation entre ces deux model est one-to-many*/

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
