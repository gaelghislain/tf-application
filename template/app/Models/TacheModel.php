<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TacheModel extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    protected $table = 'taches';
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
    public function type()
    {
        return $this->belongsTo(Type_tache::class, 'type_tache_id');
    }
}
