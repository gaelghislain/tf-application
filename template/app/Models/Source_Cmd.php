<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Source_Cmd extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*une source peut avoir plusieur commande tandis qu'une commande
    appartient à une seule source alors la renla tion entre ces model est one-to-many*/

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }


    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
