<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Attribuer_Cmd extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function commande()
    {
        return $this->belongsTo(Commande::class,'commandes_id');
    }

    public function commandes()
    {
        return $this->hasMany(Commande::class,'commandes_id');
    }

    public function equipement()
    {
        return $this->belongsTo(Equipement::class);
    }

    public function livrer_par()
    {
        return $this->hasMany(Liver_par::class);
    }
}
