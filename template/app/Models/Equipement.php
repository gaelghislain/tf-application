<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Equipement extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function user()
    {
      return $this->belongsTo(User::class, 'created_id');
    }

    public function commandes()
    {

      return $this->hasMany(Commande::class, 'equipement_id');
    }

    public function attributions()
    {

      return $this->hasMany(Attibuer_Cmd::class, 'equipement_id');
    }
}
