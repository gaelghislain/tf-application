<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Type_User extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*un type utilisateur peut avoir plusieur utilisateur
    ainsi la relation entre est one-to-many*/

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function gestionnairs()
    {
        return $this->hasMany(Gestionnair::class);
    }

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
