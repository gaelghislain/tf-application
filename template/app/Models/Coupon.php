<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coupon extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function user()
	{
        return $this->belongsTo(User::class,'created_id');
    }

    public function client(){

         return $this->belongsTo(Client::class);

    }

     public function date_ventes(){

         return $this->hasMany(Date_vente::class);

    }
}
