<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Suggestion extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*est cree par un utilisateur*/
    public function client()
    {
        return $this->belongsTo(Client::class);
    }
}
