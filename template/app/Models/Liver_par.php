<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Liver_par extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function commande()
    {
        return $this->belongsTo(Commande::class, 'commandes_id');
    }

    public function livreur()
    {
        return $this->belongsTo(Livreur::class);
    }

    public function attribution()
    {
        return $this->belongsTo(Attibuer_Cmd::class, 'attribution_id');
    }
}
