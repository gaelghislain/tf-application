<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Mode_pai extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*une commande dispose de plusieur mode de paiement
    tandis qu'une cmd appartient à un mode paiement*/

    public function commandes()
    {
        return $this->hasMany(Commande::class, 'mode_id');
    }

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
