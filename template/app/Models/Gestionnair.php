<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Gestionnair extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    public function users()
    {
        return $this->morphMany(User::class, 'usertable');
    }

    /*est cree par un utilisateur*/
	public function type_user(){
        return $this->belongsTo(Type_User::class);
    }
}
