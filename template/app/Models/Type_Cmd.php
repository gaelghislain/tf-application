<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Type_Cmd extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*une type peut avoir plusieur commande tandis qu'une commande
    appartient à une seule type alors la renlation entre ces model est one-to-many*/

    public function commandes()
    {
        return $this->hasMany(Commande::class);
    }


    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
