<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Type_Menu extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*un type menu peut avoir plusieur menu
    ainsi la relation  est one-to-many*/

    public function menus()
    {

        return $this->hasMany(Menu::class, 'id_type_menu');
    }

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }
}
