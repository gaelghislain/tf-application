<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Type_tache extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*est cree par un utilisateur*/

    protected $table = 'type__taches';
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }

    public function taches()
    {
        return $this->hasMany(TacheModel::class, 'id');
    }
}
