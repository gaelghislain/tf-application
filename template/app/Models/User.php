<?php

namespace App\Models;

use App\Traits\HasUuid;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasUuid;



    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    /* protected $fillable = [
        'name',
        'email',
        'password',
    ]; */

    protected $guarded = [];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'id' => 'string',
        'email_verified_at' => 'datetime',
    ];


    /*un utilisateur appartient
    à un type ulisateur*/
    public function type_user()
    {

        return $this->belongsTo(Type_User::class);
    }

    /*un utilisateur peut creer
    des type ulisateur*/
    public function type_users()
    {

        return $this->hasMany(Type_User::class);
    }

    /*un type utilisateur peut avoir plusieur commandes
    ainsi la relation  est one-to-many*/

    public function commands()
    {
        return $this->hasMany(Commande::class);
    }

    /*un utilisateur peut creer
    des type ulisateur*/
    public function type_menus()
    {

        return $this->hasMany(Type_User::class);
    }


    /*un utilisateur peut creer
    des type commande*/
    public function type_cmds()
    {

        return $this->hasMany(Type_Cmd::class);
    }

    /*un utilisateur peut creer
    des livreurs*/
    public function livreurs()
    {

        return $this->hasMany(Livreur::class);
    }

    /*un utilisateur peut creer
    des parametres*/
    public function parametres()
    {

        return $this->hasMany(Parametre::class);
    }

    /*un utilisateur peut creer
    des parametres*/
    public function source_cmds()
    {

        return $this->hasMany(Source_Cmd::class);
    }


    /*un utilisateur peut creer
    des modes de paiement*/
    public function mode_pais()
    {

        return $this->hasMany(Mode_pai::class);
    }


    /*un utilisateur peut creer
    des menus*/
    public function menus()
    {

        return $this->hasMany(Menu::class);
    }


    /*un utilisateur peut creer
    des clients*/
    public function clients()
    {

        return $this->hasMany(Client::class);
    }


    public function usertable()
    {
        return $this->morphTo();
    }

    /*un utilisateur peut creer
    des Anormalie*/
    public function annormalies()
    {

        return $this->hasMany(Anormalie::class);
    }

    /*un utilisateur peut creer
    des empaquetages*/
    public function empaquetages()
    {

        return $this->hasMany(Empaquetage::class);
    }

    /*un utilisateur peut creer
    des Equipement*/
    public function equipements()
    {

        return $this->hasMany(Equipement::class);
    }

    /*un utilisateur peut creer
    des Equipement*/
    public function coupons()
    {

        return $this->hasMany(Coupon::class);
    }

    /*un utilisateur peut creer
    des Equipement*/
    public function receptionnaires()
    {

        return $this->hasMany(Receptionnaire::class);
    }
}
