<?php

namespace App\Models;

use App\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Menu extends Model
{
    use HasFactory, HasUuid;

    protected $guarded = [];

    /*un menu appartient
    à un type menu*/
    public function type_menu()
    {
        return $this->belongsTo(Type_Menu::class, 'type_menu_id');
    }

    /*est cree par un utilisateur*/
    public function user()
    {
        return $this->belongsTo(User::class, 'created_id');
    }


    public function commandes()
    {
        return $this->hasMany(Commande::class, 'menu_id');
    }

    public function evaluations()
    {
        return $this->hasMany(Evaluation::class);
    }
}
