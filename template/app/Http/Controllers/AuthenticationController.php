<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Gestionnair;
use App\Models\Type_User;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    public function userLogin()
    {
        $pageConfigs = ['bodyCustomClass' => 'login-bg', 'isCustomizer' => false];

        
        return view('pages.user-login', ['pageConfigs' => $pageConfigs]);
    }

    public function register(Request $request)
    {
        // dd($request);
       
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => 'required|numeric',
            'password' => 'required|min:8',
            'type_user_id' => 'required',
            'password_confirmation' => 'required_with:password|same:password|min:8',
        ]);

        try {

            $gestionnaire = Gestionnair::create([
                'id'=>str::uuid(),
                'name' => $request->name,
                'type_user_id' => $request->type_user_id,
            ]);


            $user = new User();
            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->password = Hash::make($request->password);
            $gestionnaire->users()->save($user);

            return redirect()->route('user.list')->with('successMessage', " Enregistrement éffectué");
        } 
        catch (\Throwable $th) {
            return back()->with('alertMessage', "Enregistrement échoué !" . $th);
        }
        
    }

    

    public function userRegister()
    {
        $pageConfigs = ['bodyCustomClass' => 'register-bg', 'isCustomizer' => false];

        $types = Type_User::all();
        
        return view('pages.user-register', ['pageConfigs' => $pageConfigs, 'types' => $types]);
    }
    public function forgotPassword()
    {
        $pageConfigs = ['bodyCustomClass' => 'forgot-bg', 'isCustomizer' => false];
        return view('pages.user-forgot-password', ['pageConfigs' => $pageConfigs]);
    }
    public function lockScreen()
    {
        $pageConfigs = ['bodyCustomClass' => 'forgot-bg', 'isCustomizer' => false];

        return view('pages.user-lock-screen', ['pageConfigs' => $pageConfigs]);
    }
}
