<?php

namespace App\Actions;

use App\Models\User;
use App\Models\Client;
use App\Utils\APIErrors;
use App\Mail\PasswordMail;
use App\Mail\registerMail;
use App\Models\Gestionnair;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Carnet_Adresse;
use App\Actions\API\LoginAction;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class APIRegisterAction
{

    public static function register(Request $request)
    {

        try {
            $gestionnaire = Gestionnair::create([
                'id'=>str::uuid(),
                'name' => $request->name,
                // 'ville' => $request->ville,
                // 'civilite' => $request->civilite,
                // 'email' => $request->email,
                // 'phone' => $request->phone,
            ]);

            $user = new User();
            $user->name = $request->get('name');
            // $user->username = $request->get('username');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->password = Hash::make($request->password);
            $gestionnaire->users()->save($user);
            
            return redirect()->route('user.list')->with('successMessage', " Enregistrement éffectué");
            
        } catch (\Throwable $th) {
            return back()->with('alertMessage', "Enregistrement échoué !" . $th);
        }
    }

    

    // public static function update(Request $request)
    // {
    //     try {
    //         $user = User::where('id', Auth::user()->id)->first();
    //         $user->name = $request->name;
    //         $user->email = $request->email;
    //         $user->phone = $request->phone;
    //         $user->save();

    //         $client = Client::where('id', $user->usertable_id)->first();
    //         $client->nom_prenom = $request->name;
    //         $client->email = $request->email;
    //         $client->phone = $request->phone;
    //         $client->ville = $request->ville;
    //         $client->birthday = $request->birthday;
    //         $client->civilite = $request->civilite;
    //         $client->save();
    //         //dd('ok');

    //         if (!is_null($request->adresse || !is_null($request->description))) {
    //             $carnet = Carnet_Adresse::where('client_id', $user->usertable_id)->first();

    //             if (is_null($carnet)) {
    //                 $carnet = new Carnet_Adresse();
    //                 $carnet->client_id = $user->usertable_id;
    //                 $carnet->created_id = $user->usertable_id;
    //             }

    //             $carnet->adresse = $request->adresse;
    //             $carnet->description = $request->description;
    //             $carnet->save();
    //         }

    //         return response()->json([
    //             "success" => true,
    //             "message" => "Information de " . $request->name . " mis à jour",
    //         ], 200);
    //     } catch (\Throwable $th) {
    //         return APIErrors::singleAPIError(['Erreur' . $th]);
    //     }
    // }

    // public static function password(Request $request)
    // {
    //     try {
    //         $user = User::where('id', Auth::user()->id)->first();
    //         $user->password = bcrypt($request->password);
    //         $user->save();

    //         return response()->json([
    //             "success" => true,
    //             "message" => "Modification de mot de passe réussie",
    //         ], 200);
    //     } catch (\Throwable $th) {
    //         return APIErrors::singleAPIError(['Erreur']);
    //     }
    // }

    // public static function check(Request $request)
    // {
    //     try {
    //         $user = User::where('email', $request->email)->whereNotNull('username')->where('usertable_type', 'App\Models\Client')->first();

    //         $mailData = [
    //             'id' => $user->id,
    //             'email' => $user->email,
    //             'url' => route('login'),
    //         ];
    //         Mail::to($mailData['email'])->send(new PasswordMail($mailData));

    //         if (is_null($user)) {
    //             return APIErrors::singleAPIError(['Email introuvable']);
    //         }

    //         return response()->json([
    //             "success" => true,
    //             "message" => "Consulter votre mail pour la réinitialisation !!",
    //         ], 200);

    //         //return response()->json($user, 200);
    //     } catch (\Throwable $th) {
    //         return APIErrors::singleAPIError(['Erreur' . $th]);
    //     }
    // }

    // public static function update_password (Request $request, $id)
    // {
    //     try {
    //         $user = User::where('id', $id)->first();
    //         $user->password = bcrypt($request->password);
    //         $user->save();

    //         return response()->json([
    //             "success" => true,
    //             "message" => "Mot de passe réinitialisé !!",
    //         ], 200);
    //     } catch (\Throwable $th) {
    //         return APIErrors::singleAPIError(['Erreur' . $th]);
    //     }
    // }
}
