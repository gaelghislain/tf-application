<?php

namespace App\Actions;

use App\Models\User;
use App\Models\Client;
// use App\Utils\APIErrors;
use App\Models\Gestionnair;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class APILoginAction
{

    public static function login(Request $request)
    {
        // $remember = $request->remenber;
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        $user = User::where( 'email', $request->email)->firstOrFail();
        if(Auth::attempt($credentials)) {
            $request->session()->regenerate();
            Auth::login($user, $remember = true);

            return redirect()->intended('dashboard');
        }
 
        return back()->withErrors([
            'email' => 'Email ou mot de passe incorrect.',
        ])->onlyInput('email');
    }
}
