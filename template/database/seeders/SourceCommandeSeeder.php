<?php

namespace Database\Seeders;

use App\Models\Client;
use App\Models\Menu;
use App\Models\Mode_pai;
use App\Models\Parametre;
use App\Models\Source_Cmd;
use App\Models\Type_Cmd;
use App\Models\Type_Menu;
use App\Models\Type_User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SourceCommandeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $source__cmds = array(
            array('id' => Str::uuid(), 'libelle' => 'Whatsapp', 'est_supprimer' => '1', 'created_at' => '2018-08-09 13:59:19', 'updated_at' => '2018-08-09 13:59:19'),
            array('id' => Str::uuid(), 'libelle' => 'Appel Whatsapp', 'est_supprimer' => '1', 'created_at' => '2018-08-09 13:59:30', 'updated_at' => '2018-08-09 13:59:30'),
            array('id' => Str::uuid(), 'libelle' => 'Appel téléphonique', 'est_supprimer' => '1', 'created_at' => '2018-08-09 13:59:44', 'updated_at' => '2018-08-09 13:59:44'),
            array('id' => Str::uuid(), 'libelle' => 'Application Mobile', 'est_supprimer' => '1', 'created_at' => '2018-08-09 13:59:56', 'updated_at' => '2018-08-09 13:59:56'),
            array('id' => Str::uuid(), 'libelle' => 'Google form', 'est_supprimer' => '1', 'created_at' => '2018-08-09 14:00:15', 'updated_at' => '2018-08-09 14:00:15'),
            array('id' => Str::uuid(), 'libelle' => 'Facebook', 'est_supprimer' => '1',  'created_at' => '2018-08-09 14:00:21', 'updated_at' => '2018-09-27 20:28:49'),
            array('id' => Str::uuid(), 'libelle' => 'Linkedin', 'est_supprimer' => '1', 'created_at' => '2018-09-27 20:29:16', 'updated_at' => '2018-09-27 20:29:16'),
            array('id' => Str::uuid(), 'libelle' => 'Google+', 'est_supprimer' => '1', 'created_at' => '2018-09-27 20:30:09', 'updated_at' => '2018-09-27 20:30:36')
        );

        foreach ($source__cmds as $item) {
            Source_Cmd::create($item);
        }

        // Mode de paiement

        $mode_pais = array(
            array('id' => Str::uuid(), 'libelle' => 'TL10', 'visible' => '1', 'est_supprimer' => '1',  'created_at' => '2018-08-09 14:03:01', 'updated_at' => '2020-03-23 08:12:23', 'code' => 'Ticket'),
            array('id' => Str::uuid(), 'libelle' => 'TL5', 'visible' => '1', 'est_supprimer' => '1',  'created_at' => '2018-08-09 14:03:07', 'updated_at' => '2020-03-23 08:12:25', 'code' => 'Ticket'),
            array('id' => Str::uuid(), 'libelle' => 'ESPECES', 'visible' => '1', 'est_supprimer' => '1',  'created_at' => '2018-08-09 14:04:37', 'updated_at' => '2020-03-23 08:12:25', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'MTN Mobile Money', 'visible' => '1', 'est_supprimer' => '1',  'created_at' => '2018-08-09 14:04:55', 'updated_at' => '2020-03-20 17:41:53', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'TICKET ORA BANK', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2020-03-25 07:52:20', 'updated_at' => '2022-03-31 07:29:53', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'OFFERT', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2020-03-25 07:52:39', 'updated_at' => '2022-03-31 07:29:51', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'PP-SP', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2020-03-25 07:55:17', 'updated_at' => '2022-03-31 07:29:59', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'ABONNEE -SMP', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2020-03-25 07:57:15', 'updated_at' => '2022-03-31 07:29:58', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'TICKET SGB', 'visible' => '1', 'est_supprimer' => '1',  'created_at' => '2021-02-17 08:52:28', 'updated_at' => '2022-03-31 07:29:46', 'code' => 'Autre'),
            array('id' => Str::uuid(), 'libelle' => 'TICKET MTN SHINE 2500', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2022-03-31 07:27:25', 'updated_at' => '2022-03-31 07:29:42', 'code' => 'Ticket'),
            array('id' => Str::uuid(), 'libelle' => 'TICKET MTN SHINE 3500', 'visible' => '1', 'est_supprimer' => '1',   'created_at' => '2022-03-31 07:27:39', 'updated_at' => '2022-04-23 15:59:32', 'code' => 'Ticket')
        );

        foreach ($mode_pais as $item) {
            Mode_pai::create($item);
        }

        // Type commande

        $type__cmds = array(
            array('id' => Str::uuid(), 'libelle' => 'Africian', 'est_supprimer' => '0', 'created_at' => '2018-08-09 13:58:37', 'updated_at' => '2018-08-09 14:00:58', 'pu' => '0'),
            array('id' => Str::uuid(), 'libelle' => 'Béninois', 'est_supprimer' => '0', 'created_at' => '2018-08-09 13:58:44', 'updated_at' => '2018-08-09 14:00:56', 'pu' => '0'),
            array('id' => Str::uuid(), 'libelle' => 'Français', 'est_supprimer' => '0', 'created_at' => '2018-08-09 13:58:50', 'updated_at' => '2018-08-09 14:00:53', 'pu' => '0'),
            array('id' => Str::uuid(), 'libelle' => 'Asiatique', 'est_supprimer' => '0', 'created_at' => '2018-08-09 13:59:02', 'updated_at' => '2018-08-09 14:00:51', 'pu' => '0'),
            array('id' => Str::uuid(), 'libelle' => 'STANDARD PACK', 'est_supprimer' => '1', 'created_at' => '2018-08-09 14:01:03', 'updated_at' => '2018-12-06 12:59:41', 'pu' => '3500'),
            array('id' => Str::uuid(), 'libelle' => 'Resistance + Boisson', 'est_supprimer' => '0', 'created_at' => '2018-08-09 14:01:09', 'updated_at' => '2018-08-29 14:54:12', 'pu' => '2500'),
            array('id' => Str::uuid(), 'libelle' => 'Resistance + Dessert + Boisson', 'est_supprimer' => '0', 'created_at' => '2018-08-09 14:01:17', 'updated_at' => '2018-08-29 13:18:37', 'pu' => '3000'),
            array('id' => Str::uuid(), 'libelle' => 'Entrée + Resistance + Boisson', 'est_supprimer' => '0', 'created_at' => '2018-08-29 13:18:58', 'updated_at' => '2018-09-27 20:27:55', 'pu' => '3000'),
            array('id' => Str::uuid(), 'libelle' => 'Spéciale week-and', 'est_supprimer' => '0', 'created_at' => '2018-09-27 20:27:30', 'updated_at' => '2018-10-06 18:22:58', 'pu' => '10000'),
            array('id' => Str::uuid(), 'libelle' => 'SMALL PACK', 'est_supprimer' => '1', 'created_at' => '2018-12-06 12:59:22', 'updated_at' => '2018-12-06 12:59:22', 'pu' => '2500'),
            array('id' => Str::uuid(), 'libelle' => 'PACK ANNIVERSAIRE', 'est_supprimer' => '1', 'created_at' => '2019-03-19 22:14:24', 'updated_at' => '2019-03-19 22:14:24', 'pu' => '5500'),
            array('id' => Str::uuid(), 'libelle' => 'PACK SALADE DINER', 'est_supprimer' => '1', 'created_at' => '2019-07-23 22:46:34', 'updated_at' => '2019-07-23 22:46:34', 'pu' => '2500'),
            array('id' => Str::uuid(), 'libelle' => 'Jardinière', 'est_supprimer' => '1', 'created_at' => '2021-02-16 13:37:10', 'updated_at' => '2021-02-16 13:38:42', 'pu' => '1000'),
            array('id' => Str::uuid(), 'libelle' => 'Pack interne 1000F', 'est_supprimer' => '1', 'created_at' => '2021-02-16 13:38:48', 'updated_at' => '2021-02-16 13:39:00', 'pu' => '1000'),
            array('id' => Str::uuid(), 'libelle' => 'Pack interne 1500F', 'est_supprimer' => '1', 'created_at' => '2021-02-16 13:39:05', 'updated_at' => '2021-02-16 13:39:14', 'pu' => '1500')
        );

        foreach ($type__cmds as $item) {
            Type_Cmd::create($item);
        }

        // Type menu
        $type__menus = array(
            array('id' => Str::uuid(), 'libelle' => 'Africain', 'est_supprimer' => '1', 'created_at' => '2018-08-09 13:46:08', 'updated_at' => '2018-08-09 14:02:16'),
        );

        foreach ($type__menus as $item) {
            Type_Menu::create($item);
        }

        // Menu

        $menus = array(
            array('id' => Str::uuid(), 'entrer' => 'Salade de betterave au thon', 'desc_Entrer' => NULL, 'resistance' => 'Poisson braisé', 'desc_Resistance' => NULL, 'accompagnement' => 'Riz au gras', 'dessert' => 'Salade de fruits', 'desc_Dessert' => NULL, 'boisson' => 'Orange-ananas', 'desc_Boisson' => NULL, 'active' => '0', 'date' => '2018-10-08', 'type_menu_id' => Type_Menu::all()[0]->id, 'created_at' => '2018-09-27 16:18:17', 'updated_at' => '2018-10-09 00:51:54', 'est_supprimer' => '1', 'image' => '5bb9e4c4611f7_1538909380.png'),
        );

        foreach ($menus as $item) {
            Menu::create($item);
        }

        $parametres = array(
            array('id' => Str::uuid(), 'notif_cmd' => 'Commande acceptée.', 'notif_mis_livaison' => 'Commande mise en livraison.', 'notif_livrer' => 'Commande livrée.', 'notif_rejet' => 'Quota atteint pour ce menu.', 'quota' => '10', 'quota_courant' => '10', 'created_at' => '2018-08-09 14:06:38', 'updated_at' => '2019-05-02 19:11:58', 'est_supprimer' => '1', 'heure_min' => '11:00:00', 'heure_max' => '16:00:00', 'ref_fact' => 'DG/DTF')
        );

        foreach ($parametres as $item) {
            Parametre::create($item);
        }
/*
        $carnet__adresses = array(
            array('id' => Str::uuid(),'adresse' => 'Zongo','description' => 'En face de royal hôtel.','est_supprimer' => '1','client_id' => Client::all()[0]->id, 'created_at' => '2018-10-07 11:11:21','updated_at' => '2018-10-09 09:35:41')
        );

        foreach ($parametres as $item) {
            Parametre::create($item);
        } */

        $type__users = array(
            array('id' => Str::uuid(),'libelle' => 'Admin','est_supprimer' => '1','created_at' => '2018-08-09 13:57:56','updated_at' => '2018-08-09 13:57:56'),
            array('id' => Str::uuid(),'libelle' => 'Gestionnaire','est_supprimer' => '1','created_at' => '2018-08-09 13:58:07','updated_at' => '2018-09-27 20:25:14'),
            array('id' => Str::uuid(),'libelle' => 'Empaqueteur','est_supprimer' => '1','created_at' => '2018-08-09 13:58:18','updated_at' => '2018-08-09 13:58:18'),
            array('id' => Str::uuid(),'libelle' => 'Cuisine','est_supprimer' => '1','created_at' => '2018-09-27 20:24:26','updated_at' => '2018-09-27 20:25:39'),
            array('id' => Str::uuid(),'libelle' => 'Statistique','est_supprimer' => '1','created_at' => '2019-03-09 17:28:35','updated_at' => '2019-03-09 17:28:35'),
            array('id' => Str::uuid(),'libelle' => 'Commerciale','est_supprimer' => '1','created_at' => '2020-01-16 13:37:24','updated_at' => '2020-01-16 13:37:24')
          );

          foreach ($type__users as $item) {
            Type_User::create($item);
        }
    }
}
