<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parametres', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('notif_cmd');
            $table->string('notif_mis_livaison');
            $table->string('notif_livrer');
            $table->string('notif_rejet');
            $table->integer('quota');
            $table->integer('quota_courant');
            $table->boolean('est_supprimer');
            $table->time('heure_min');
            $table->time('heure_max');
            $table->string('ref_fact');
            $table->uuid('created_id')->nullable()->unsigned(false);
            $table->uuid('modificateur_id')->nullable()->unsigned(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parametres');
    }
};
