<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commandes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('menu_id')->unsigned(false);
            $table->uuid('client_id')->unsigned(false);
            $table->uuid('carnet_adresse_id')->unsigned(false);
            $table->uuid('source_id')->unsigned(false);
            $table->uuid('mode_id')->unsigned(false);
            $table->uuid('type_id')->unsigned(false);
            $table->dateTime('date_heure_livraison')->nullable();
            $table->integer('nbr_pack')->nullable();
            $table->boolean('est_accepter')->default(false);
            $table->text('motif_rejet')->nullable();
            $table->boolean('est_livrer')->default(false);
            $table->boolean('cmd_attente')->default(false);
            $table->boolean('cmd_annule')->default(false);
            $table->uuid('receptionnaire_id')->unsigned(false);
            $table->uuid('created_id')->nullable()->unsigned(false);
            $table->uuid('modificateur_id')->nullable()->unsigned(false);
            $table->boolean('facture')->default(false);
            $table->boolean('est_supprimer')->default(false);
            $table->text('commentaire')->nullable();
            $table->boolean('est_attribuer')->default(false);
            $table->text('commentaire_client')->nullable();
            $table->boolean('est_mise_livraison')->default(false);
            $table->dateTime('date_mise_livraison')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commandes');
    }
};
