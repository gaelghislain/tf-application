<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mode_pais', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('libelle')->nullable();
            $table->boolean('visible')->default(false);
            $table->boolean('est_supprimer')->nullable();
            $table->uuid('created_id')->nullable()->unsigned(false);
            $table->uuid('modificateur_id')->nullable()->unsigned(false);
            $table->string('code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mode_pais');
    }
};
