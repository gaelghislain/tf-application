<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nom_prenom')->nullable();
            $table->string('ville')->nullable();
            $table->date('birthday')->nullable();
            $table->string('civilite')->nullable();
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->boolean('est_supprimer')->default(true);
            $table->integer('nbr_coupon')->default(0);
            $table->uuid('created_id')->unsigned(false)->nullable();
            $table->uuid('modificateur_id')->unsigned(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
