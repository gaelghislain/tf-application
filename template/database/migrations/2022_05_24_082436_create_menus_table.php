<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('entrer');
            $table->string('desc_Entrer')->nullable();
            $table->string('resistance');
            $table->string('desc_Resistance')->nullable();
            $table->string('dessert');
            $table->string('desc_Dessert')->nullable();
            $table->string('boisson');
            $table->string('desc_Boisson')->nullable();
            $table->string('accompagnement');
            $table->string('image');
            $table->date('date');
            $table->boolean('active');
            $table->boolean('est_supprimer');
            $table->uuid('created_id')->nullable()->unsigned(false);
            $table->uuid('modificateur_id')->nullable()->unsigned(false);
            $table->uuid('type_menu_id')->unsigned(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
};
