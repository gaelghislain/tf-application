<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribuer__cmds', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('command_id')->unsigned(false);
            $table->uuid('equipement_id')->unsigned(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attribuer__cmds');
    }
};
