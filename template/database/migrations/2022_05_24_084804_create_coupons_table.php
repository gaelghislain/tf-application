<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('libelle');
            $table->integer('seuil_alerte');
            $table->integer('nbr_feuillet');
            $table->integer('code');
            $table->boolean('use')->default(false);
            $table->boolean('est_supprimer');
            $table->uuid('created_id')->unsigned(false);
            $table->uuid('modificateur_id')->unsigned(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
};
