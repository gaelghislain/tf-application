<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carnet__adresses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('adresse')->nullable();
            $table->string('description')->nullable();
            $table->uuid('client_id')->unsigned(false);
            $table->string('est_supprimer')->nullable();
            $table->uuid('created_id')->unsigned(false)->nullable();
            $table->uuid('modificateur_id')->unsigned(false)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carnet__adresses');
    }
};
