<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receptionnaires', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name_prenom');
            $table->string('ville');
            $table->string('civilite');
            $table->string('telepphone')->unique();
            $table->string('email')->unique();
            $table->string('signature');
            $table->boolean('est_supprimer');
            $table->uuid('created_id')->unsigned(false);
            $table->uuid('modificateur_id')->unsigned(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receptionnaires');
    }
};
