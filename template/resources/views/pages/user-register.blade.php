{{-- layout --}}
@extends('layouts.fullLayoutMaster')

{{-- page title --}}
@section('title','User Register')

{{-- page style --}}
@section('page-style')
<link rel="stylesheet" type="text/css" href="{{asset('css/pages/register.css')}}">
@endsection

{{-- page content --}}
@section('content')
<div id="register-page" class="row">
  <div class="col s12 m6 l4 z-depth-4 card-panel border-radius-6 register-card">
    <form class="login-form" method="POST" action="{{ route('register') }}">
      @csrf
      <div class="row">
        <div class="input-field col s12">
          <img src="https://web.topfood.bj/images/LogotTF.png" alt="LogoTF" style="width: 100%" />
          <h5 class="ml-4">Inscription</h5>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">person_outline</i>
          <input id="name" name="name" type="text" class="@error('name') is-invalid @enderror">
          <label for="name" class="center-align">Nom d'utilisateur</label>
          @error('name')
          <small class="red-text ml-7" role="alert">
            {{ $message }}
          </small>
          @enderror
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">mail_outline</i>
          <input id="email" name="email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" class="@error('email') is-invalid @enderror">
          <label for="email">Email</label>
          @error('email')
          <small class="red-text ml-7" role="alert">
            {{ $message }}
          </small>
          @enderror
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">phone_outline</i>
          <input id="phone" name="phone" type="tel" class="@error('phone') is-invalid @enderror">
          <label for="phone">Numéro de téléphone</label>
          @error('phone')
          <small class="red-text ml-7" role="alert">
            {{ $message }}
          </small>
          @enderror
        </div>
      </div>

      <div class="row margin">
        <div class="input-field col s12">
          <div class="input-field">
            <i class="material-icons prefix pt-2">assignment_outline</i>
            <select class="select2" id="select_role" style="cursor:pointer;" name="type_user_id" class="@error('type_user_id') is-invalid @enderror">
              <option value=""></option>
              @foreach($types as $type)
              <option value="{{$type->id}}">{{$type->libelle}}</option>
              @endforeach
            </select>
            <label for="select_role">Rôle</label>

            @error('type_user_id')
            <small class="red-text ml-7" role="alert">
              {{ $message }}
            </small>
            @enderror
          </div>
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <input id="password" name="password" type="password" class="@error('password') is-invalid @enderror">
          <label for="password">Mot de passe</label>
          @error('password')
          <small class="red-text ml-7" role="alert">
            {{ $message }}
          </small>
          @enderror
        </div>
      </div>
      <div class="row margin">
        <div class="input-field col s12">
          <i class="material-icons prefix pt-2">lock_outline</i>
          <input id="password_confirmation" name="password_confirmation" type="password" class="@error('password_confimation') is-invalid @enderror">
          <label for="password_confirmation">Confirmer le mot de passe</label>
          @error('password_confirmation')
          <small class="red-text ml-7" role="alert">
            {{ $message }}
          </small>
          @enderror
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <button type="submit" class="btn waves-effect waves-light border-round gradient-45deg-purple-deep-orange col s12">S'inscrire</button>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <!-- <p class="margin medium-small"><a href="{{asset('user-login')}}">Already have an account? Login</a></p> -->
        </div>
      </div>
    </form>
  </div>
</div>
@endsection